import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // variabel penampung warna
  List<Color> colors = [Colors.amber, Colors.red, Colors.blue];
//  watna default dari index ke nol
  int currentColorIndex = 0;

  int _counter = 0;

  void ubahWarna() {
    setState(() {
      // bismillah
      // timpa variabel currentColorIndex berdasarkan currentColorIndex tapi ++
      currentColorIndex = (currentColorIndex + 1) % colors.length;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.amber,
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'Rouf Majidin',
            ),
            // widget dinamis warna
            Container(
              height: 200,
              width: 200,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16),
                // warna berdasarkan variabel diatas
                color: colors[currentColorIndex],
              ),
            )
          ],
        ),
      ),
      // tombol ubah warna
      floatingActionButton: FloatingActionButton(
        onPressed: ubahWarna,
        tooltip: 'Ubah Warna',
        child: const Icon(Icons.edit),
      ),
    );
  }
}
